#!/usr/bin/python3
#Phonic GPL3
#Copyright (C) 2019 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import subprocess
import threading
import time
import operator
from gi.repository import Gtk


global ctrl_box
global info_box

global skip_back
global skip_forward
global play_pause
global play_pause_txt
global file_name
global book_name
global selected #like book name but not the full path
global FILES
global play_process
global current_file
global current_location
global time_left
global force_stop
global book_mark
global file_progress
global all_progress
global last_progress #displayed progress
global MAX_STR_SIZE
global file_times

MAX_STR_SIZE = 45

play_pause_txt = "▻"
FILES = []
file_times = {}
play_process = ""
current_file = ""
current_location = ""
time_left = ""
force_stop = False
book_mark = ""
last_progress = 0

window = Gtk.Window()
window.connect("delete-event", Gtk.main_quit)
FILES
def scan_dir(folder):
    files = {}
    possible_extensions = {}
    #find files
    for dirpath, dnames, fnames in os.walk(folder):
        for f in fnames:
            files[f] = os.path.join(dirpath, f)
            if '.' in f:
                extension = f.split('.')[-1]
                if extension in possible_extensions:
                    possible_extensions[extension] = possible_extensions[extension] + 1
                else:
                    possible_extensions[extension] = 1
    #Find most common extension
    extension = max(possible_extensions.items(), key=operator.itemgetter(1))[0]
    print("Extension: ." + extension)
    file_list = []
    for f in files.keys():
        if f.endswith(extension):
            file_list.append(f)
        else:
            print("File not use for audio: " + f)
    
    #sort
    #file_list = list(files.keys())
    file_list.sort()
    return_in_order = []
    file_times = {}
    total_abook_time = 0
    #return file paths
    for file_name in file_list:
        print(".",end='')
        fn = files[file_name]
        f_len = get_len(fn)
        return_in_order.append(fn)
        file_times[fn] = f_len
        total_abook_time = total_abook_time + f_len
        #print(file_name)
    print("Total time: " + str(total_abook_time))
    return (return_in_order,file_times, total_abook_time)

def play():
    global current_file
    global current_location
    global force_stop
    global time_left
    global file_progress
    global all_progress
    global last_progress
    global play_pause

    if current_file == "":
        print("Open book first")
        return []
    load_book_mark()
    cmd = ["play"]
    cmd.append(current_file)
    output_cmd = cmd[0] + ' "' + cmd[1] + '" '
    if current_location != "":
        output_cmd = output_cmd + "trim " + current_location
        cmd.append("trim")
        cmd.append (current_location)
    
    print(output_cmd)
    
    
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    line = ""
    book_mark_age = 0
    while True:
        if force_stop:
            process.kill()
            force_stop = False
            break
        #output = process.stdout.read(1)
        #lol, output is all on stderr...
        if process.poll() is None:
            output = process.stderr.read(1).decode()
        else:
            print('Audio over')
            print('Return: ' + str(process.returncode))
            #Done with this file, open next
            if process.returncode == 0:
                new_file_index = FILES.index(current_file) + 1
                if len(FILES) > new_file_index:
                    #play next file
                    current_file = FILES[new_file_index]
                    current_location = "00:00:00.00"
                    time_left = sec_2_stamp(get_len(current_file))
                    write_book_mark()
                    play()
                    break
                else:
                    #book is over
                    print ("The End")
                    current_file = FILES[0]
                    current_location = "00:00:00.00"
                    time_left = sec_2_stamp(get_len(current_file))
                    write_book_mark()
                    play_pause.set_label("▻")
                    break
            else:
                print("Error reading file: " + current_file)
                print("Returncode: " + str(process.returncode))
                print(str(cmd) + "\n")
                print(time_left)
                return
        if output == "\r":
            if line != "":
                if "In:" in line:
                    time_stamp = line.split("%")[-1].split("[")[0].strip()
                    time_left = line.split("[")[1].split("]")[0].strip()
                    current_location = time_stamp
                    book_mark_age = book_mark_age + 1
                    progress, total_progress = get_progress()
                    if abs(last_progress - progress) > .01:
                        file_progress.set_fraction(progress)
                        all_progress.set_fraction(total_progress)
                        last_progress = progress
                        #print("At: " + current_location)
                        #print("Left: " + time_left + "\n")
                #30 = 15 sec.
                if book_mark_age >= 30:
                    write_book_mark()
                    book_mark_age = 0
                line = ""
        else:
            line = line + output
      
    rc = process.poll()
    return rc

def get_progress():
    global current_location
    global time_left
    global total_abook_time
    global file_times
    global current_file
    global FILES
   
    #file progress
    total_time = stamp_2_sec(current_location)
    current_in_sec = total_time
    total_time = total_time + stamp_2_sec(time_left)
   
    if total_time == 0:
        progress = 0
    else:
        progress = (1/total_time) * current_in_sec
    
    
    #total progress
    total_sec = 0
    file_index = FILES.index(current_file)
    for done_file in range(0, file_index):
        done_file = FILES[done_file]
        total_sec = total_sec + file_times[done_file]
        
    total_sec = total_sec + stamp_2_sec(current_location)
    
    total_progress = (1/total_abook_time) * total_sec
    if (progress < 1 and progress >= 0) and (total_progress < 1 and total_progress >= 0):
        return progress, total_progress
    else:
        return 0, 0


def write_book_mark():
    global book_name
    global current_location
    global current_file
    book_mark = book_name + "/book_mark.txt"
    with open(book_mark, "w+") as fh:
        fh.write(current_file + '~' + current_location)

def load_book_mark():
    global current_location
    global current_file
    global book_name
    global time_left
    
    book_mark = book_name + "/book_mark.txt"
    if not os.path.exists(book_mark):
        current_file = FILES[0]
        file_name.set_label(current_file.split("/")[-1][:MAX_STR_SIZE])
        current_location = "00:00:00.00"
        return
    with open(book_mark, "r") as fh:
        data = fh.readlines()
        if data != []:
            data = data[0].split('~')
        else:
            #print("STR" +str( data))
            current_file = FILES[0]
            file_name.set_label(current_file.split("/")[-1][:MAX_STR_SIZE])
            current_location = "00:00:00.00"
            return
        current_file = data[0]
        file_name.set_label(current_file.split("/")[-1][:MAX_STR_SIZE])
        current_location = data[1].strip()
        time_left = sec_2_stamp(get_len(current_file) - stamp_2_sec(current_location))
        #print("BookMark: " + str(data))
       

def pause_it(widget):
    global play_pause
    global play_process
    global force_stop
  
    if play_pause.get_label() == "||":
        play_pause.set_label("▻")
        write_book_mark()
        if play_process != "":
            force_stop = True
            #play_process.join()
    else:
        #PLAY
        play_pause.set_label("||")
        play_process = threading.Thread(target=play)
        play_process.daemon = True
        play_process.start()

def stamp_2_sec(stamp):
    sec = 0
    multiplier = 3600 #sec in an hour
    for part in stamp.split(":"):
        sec = sec + (float(part) * multiplier)
        multiplier = multiplier/60
    return sec

def sec_2_stamp(sec):
    #convert format:
    hours = int(sec/3600)
    string_buff = "{:02d}".format(hours)
    sec = sec - (hours * 3600)

    minutes = int(sec/60)
    sec = sec - (minutes * 60)
    string_buff = string_buff + ":{:02d}".format(minutes)

    string_buff = string_buff + ":" + "{:.2f}".format(sec)
    return (string_buff)

def get_len(file_name):
    #soxi -D
    cmd = ["soxi","-D", file_name]
    return(float(subprocess.check_output(cmd).decode().strip()))
   
def load_next(forward=True, offset=0):
    global current_location
    global time_left #todo check skip forward
    global current_file
    global file_name
   
    if forward:
        new_file_index = FILES.index(current_file) + 1
        if new_file_index >= len(FILES):
            #book over
            current_file = FILES[0]
            file_name.set_label(current_file.split("/")[-1][:MAX_STR_SIZE])
            current_location = "00:00:00.00"
            time_left = sec_2_stamp(get_len(current_file))
        else:
            current_file = FILES[new_file_index]
            file_name.set_label(current_file.split("/")[-1][:MAX_STR_SIZE])
            current_location = sec_2_stamp(offset)
            print("Setup location to: " + str(current_location))
            time_left = time_left = sec_2_stamp(get_len(current_file))
    else:
        new_file_index = FILES.index(current_file) - 1
        if new_file_index == -1:
            new_file_index = 0
        current_file = FILES[new_file_index]
        file_len = get_len(current_file) #TODO read from file_len
        current_location_s = file_len + offset
        current_location = sec_2_stamp(current_location_s)
        time_left = sec_2_stamp(file_len - current_location_s)
        sec_2_stamp(file_len)

        print('New current_location: ' + str(current_location))


def time_stamp_change(diff):
    global current_location
    global time_left #todo check skip forward
    global current_file
    global last_progress
    global all_progress
    global file_progress
   
    sec = stamp_2_sec(current_location)
    #chage seconds
    sec = sec + diff
   
    #change back one file
    if sec < 0:
        #TODO add def to change file
        print("FILE CHNAGE1")
        load_next(forward=False, offset=sec)
        #time.sleep(.5)
    else:
        if time_left == "":
            print("Cannot skip safely")
            return
        #change forward one file
        if diff >= stamp_2_sec(time_left) and diff > 0:
            sec = diff - stamp_2_sec(time_left)
            print("FILE CHNAGE2")
            load_next(offset=sec)
        else:
            current_location = sec_2_stamp(sec)
            time_left = sec_2_stamp(get_len(current_file) - sec)
            #time.sleep(.5)

    #convert format:
    #current_location = sec_2_stamp(sec)
    write_book_mark()
    progress, total_progress = get_progress()
    last_progress = progress
    file_progress.set_fraction(progress)
    all_progress.set_fraction(total_progress)
  
def track_skip(widget):
    global current_location
    global force_stop
    global file_progress
    global all_progress
    global last_progress
    
    label = widget.get_label()
    
    #stop playing audio
    if play_pause.get_label() == "||":
        force_stop = True
        #wait for exit
        while force_stop:
            time.sleep(.1)
    
    #change location
    if label == "|<":
        if stamp_2_sec(current_location) < 5:
            load_next(forward=False)
    if label == ">|":
        load_next()
    
    current_location = "00:00:00.00"
    write_book_mark()
    progress, total_progress = get_progress()
    last_progress = progress
    file_progress.set_fraction(progress)
    all_progress.set_fraction(total_progress)
    
    if play_pause.get_label() == "||":
        #PLAY if needed
        play_pause.set_label("||")
        play_process = threading.Thread(target=play)
        play_process.daemon = True
        play_process.start()
    print("Skip forward")
    
def skip(widget):
    global force_stop
    global current_file
    global current_location
    global play_pause
   
    time_change = 0
    label = widget.get_label()
    if label == "<":
        time_change = -10
    if label == "<<":
        time_change = -60
    if label == ">":
        time_change = 10
    if label == ">>":
        time_change = 60
    print(current_location)
    if play_pause.get_label() == "||":
        force_stop = True
        #wait for exit
        while force_stop:
            time.sleep(.1)
        #PLAY
        time_stamp_change(time_change)
        play_pause.set_label("||")
        play_process = threading.Thread(target=play)
        play_process.daemon = True
        play_process.start()
    print("Skip " + str(time_change))

def select_a_book(widget):
    global file_name
    global book_name
    global pages
    global selected
    global FILES
    global current_file
    global current_location
    global book_mark
    global file_times
    global total_abook_time
    
    dialog = Gtk.FileChooserDialog("Select AudioBook:", None, Gtk.FileChooserAction.SELECT_FOLDER, ("Cancel", Gtk.ResponseType.CANCEL, "Open", Gtk.ResponseType.OK))
 
    response = dialog.run()
    if response == Gtk.ResponseType.OK:
        print("ok")
        path = dialog.get_filename()
        name = path.strip("/").split("/")[-1].strip()
        book_name = path
        selected.set_label(name[:MAX_STR_SIZE])
        FILES, file_times, total_abook_time = scan_dir(path)
        current_file = FILES[0]
        load_book_mark()
        #setup progress bars
        progress, total_progress = get_progress()
        last_progress = progress
        file_progress.set_fraction(progress)
        all_progress.set_fraction(total_progress)

        print(dialog.get_filename())
    if response == Gtk.ResponseType.CANCEL:
        print("Cancel")
    dialog.destroy()

#holds everything
content = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)

select_file = Gtk.Button("Open AudioBook")
select_file.connect("clicked", select_a_book)
select_file.set_halign(Gtk.Align.END)

selected = Gtk.Label()
selected.set_label("No book set")
selected.set_halign(Gtk.Align.END)
selected.set_hexpand(True)

# Player stuff
file_name = Gtk.Label()
file_name.set_label("No book set")
file_name.set_halign(Gtk.Align.END)

file_progress = Gtk.ProgressBar()
all_progress = Gtk.ProgressBar()

track_back = Gtk.Button(label="|<")
track_back.connect("clicked", track_skip)

skip_back =  Gtk.Button(label="<<")
skip_back.connect("clicked", skip)

skip_back_small =  Gtk.Button(label="<")
skip_back_small.connect("clicked", skip)

play_pause =  Gtk.Button(label=play_pause_txt)
play_pause.connect("clicked", pause_it)

skip_forward_small =  Gtk.Button(label=">")
skip_forward_small.connect("clicked", skip)

skip_forward =  Gtk.Button(label=">>")
skip_forward.connect("clicked", skip)

track_forward = Gtk.Button(label=">|")
track_forward.connect("clicked", track_skip)

info_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
info_box.pack_start(file_name, True, False, 0)
info_box.pack_start(file_progress, True, True, 0)
info_box.pack_start(all_progress, True, True, 0)
info_box.set_hexpand(True)

#holds player controls
ctrl_grid = Gtk.Grid(column_homogeneous=True,
                         column_spacing=10,
                         row_spacing=10)
#left, top, width, height
ctrl_grid.attach(play_pause, 4, 0, 2, 3)
ctrl_grid.attach(track_back, 0, 0, 1, 1)

ctrl_grid.attach(skip_back, 0, 1, 4, 1)
ctrl_grid.attach(skip_back_small, 1, 2, 3, 1)
ctrl_grid.attach(skip_forward_small, 6, 2, 3, 1)
ctrl_grid.attach(skip_forward, 6, 1, 4, 1)
ctrl_grid.attach(track_forward, 9, 0, 1, 1)
#ctrl_grid.attach(ctrl_box, 0, 1, 1, 1)



#setup main window
main_area = Gtk.Stack()
main_area.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
main_area.set_transition_duration(500)

main_grid = Gtk.Grid(row_spacing=50)
main_grid.attach(info_box, 0, 0, 1, 1)
main_grid.attach(ctrl_grid, 0, 1, 1, 1)

#setup selection window
select_grid = Gtk.Grid()
select_grid.attach(selected,    0, 0, 1, 1)
select_grid.attach(select_file, 0, 1, 1, 1)

#add pages to switcher
main_area.add_titled(main_grid, "", "Play")
main_area.add_titled(select_grid, "", "Select Book")
pages = Gtk.StackSwitcher()
pages.set_stack(main_area)

content.pack_start(pages, expand=False, fill=True, padding =50)
content.pack_start(main_area, True, True, 0)

window.set_border_width(20)
window.set_default_size(360,720)

#add backgournd:
#bg = get_bg()
#if bg != "":
#        overlay = Gtk.Overlay()
#        window.add(self.overlay)
#        background = Gtk.Image.new_from_file(bg)
#        overlay.add(self.background)

window.add(content)

#display it all
window.show_all()
Gtk.main()
